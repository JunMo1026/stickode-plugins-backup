<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once("/nginx/codecopy/backend/header/util.php");
header("Content-Type: application/json; charset=UTF-8"); // 이 코드 추가하면, 브라우저에서 json 형태로 볼 수 있음


if(isset($_POST['id']) && isset($_POST['pwd'])) {
    $id=$_POST['id'];
    $pwd=$_POST['pwd'];

    //로그인 데이터가 정상적으로 들어왔을 경우
    if($id!="" && $pwd!=""){
        $password_hash=hash("sha256", $pwd);
        //아이디 확인하는 쿼리문 실행 (아이디에 맞는 no(index)값을 가져오는 쿼리문)
//    echo "id : $id  + pwd : $pwd";
        $dbHandler = new DBHandler();
        $dbHandler->setTable("user"); // 테이블 이름
        $columns = "no"; // 가져올 컬럼
        $where = "status='1' && id='$id' && pw='$password_hash'"; // where 조건
        $dbHandler->setColumns($columns);
        $result = $dbHandler->gets($where,"");

        //user no(index)값 (※플러그인에 저장해서 데이터 불러올때 쓸 예정)
        $user_index = $result["list"][0]["no"];
        // user의 no(index)가 있을 경우(로그인 성공)
        if($user_index !=""){

            //user의 id, no 값, user 가 즐겨찾기한 코드들도 전부 보내기
//            print_r($user_index);

            $dbHandler->setTable("mycode"); // 테이블 이름
            $columns_1 = "code_no"; // 가져올 컬럼
            $where_1 = "user_no='$user_index'"; // where 조건
            $dbHandler->setColumns($columns_1);
            $result_1 = $dbHandler->gets($where_1,"");

            $user_code_no = $result_1["list"][0];
            //유저가 즐겨찾기한 코드가 있을 경우
            if ($user_code_no !=""){
                $count_user_code_no = count($result_1["list"]); //유저가 즐겨찾기한 코드 갯수
                $codes = array();


                // 유저가 즐겨찾기한 코드의 번호를 가져와 WHERE 부분의 쿼리문을 만들어준다.
                //ex:) 만약 유저가 1번, 2번, 3번 코드를 즐겨찾기했다면
                //code_no='1' OR code_no='3' OR code_no='3'
                // 위의 형태로 만들어져서 쿼리문의 WHERE 부분에 넣어준다.
                $code_no_query = "";
                for($i=0; $i<$count_user_code_no; $i++){
                    $code_no = $result_1["list"][$i]["code_no"];
                    if($i != $count_user_code_no-1){
                        $code_no_query = "$code_no_query code_no='$code_no' OR ";
                    }else{
                        $code_no_query = "$code_no_query code_no='$code_no' ";
                    }
                }
//                print_r($code_no_query);
                $dbHandler->setTable("code_component"); // 테이블 이름
                $columns_2 = "language,source"; // 가져올 컬럼
                $dbHandler->setColumns($columns_2);
                $where_2 = "status='1' && $code_no_query"; // where 조건
                $result_2 = $dbHandler->gets($where_2,"");

//                print_r($result_2);

                $result_count = count($result_2["list"]);
                $response_data = array();
                $idx = -1;

                for ($k=0; $k<$result_count; $k++) {
                    $string_code = $result_2["list"][$k]["source"];
                    $language = $result_2["list"][$k]["language"];

                    $xml = simplexml_load_string($string_code);

                    $title = $xml['title'];


                    $plain_count = $xml->plain->count();
                    $keycode_count =$xml->keycode->count();
                    if($keycode_count !=0){
                        $idx++;
                    }
//plain 태그의 index 값에 맞춰 plain태그와 keycode 태그 값을 순서대로 추출함
                    for ($i = 0; $i < $keycode_count; $i++) {

//    $plain_code = $xml->plain[$i];
                        $key_code = $xml->keycode[$i];

                        /**
                         * $start_pos , $finish_pos
                         * 이 두개의 변수를 주석의 시작과 끝으로 세팅해주면 된다
                         * 자바일 경우 // , \n
                         * xml 방식의 언어일 경우 <!-- , -->
                         * DB에서 코드 가져올때, 언어도 가져와서 언어별로 if else 문 만들어주기
                         *
                         *
                         */
                        $start_pos = "//";
                        $finish_pos = "\n";

                        //주석 제거한 plain 태그값을 echo
                    //    deleteComment($start_pos,$finish_pos,$plain_code);
                        //주석 제거한 keycode 값을 echo
                        $source_without_comment = deleteCommentt($start_pos, $finish_pos, $key_code);
                        $no_attr = $xml->keycode[$i]['no'];
                        $des_attr = $xml->keycode[$i]['description'];



                        $response_data[$idx][$i]["user_idx"] = $user_index;
                        $response_data[$idx][$i]["title"]= (string) $title;
                        $response_data[$idx][$i]["lan"]= $language;
                        $response_data[$idx][$i]["key_no"]= (string) $no_attr;
                        $response_data[$idx][$i]["description"]= (string) $des_attr;
                        $response_data[$idx][$i]["source"]= (string) $source_without_comment;

                        // 주석 제거하지 않은 plain 태그값을 echo
//    echo $plain_code;
                        // 주석 제거하지 않은 keycode 태그값을 echo
//    echo $key_code;

                        /**
                         * 웹으로 데이터 보낼때는 주석을 제거한 코드와 제거하지 않은 코드를 모두 보내야한다.
                         *
                         * 이유 => 웹에서는 코드에 대한 설명을 킬 땐 주석이 있는 코드를 보여주고
                         *                  코드에 대한 설명을 끌 땐 주석이 없는 코드를 보여준다.
                         */

                    }



                }

                //description 합치기 부분
                // 만약 keycode 태그의 no(index)가 같다면 description 이 같아야 한다.
                // DB에서는 no가 같은 keycode태그의 description 값은 공백이기에 합쳐줘야한다.
                for($i = 0; $i < count($response_data); $i++){

                    for ($k = 0; $k < count($response_data[$i]); $k++){
                        if( $response_data[$i][$k]["description"]==""){
                            $a = $response_data[$i][$k]["key_no"];
                            $b = $response_data[$i][$k]["source"];
                            for($t = 0; $t < $k; $t++){
                                if($response_data[$i][$t]["key_no"] == $response_data[$i][$k]["key_no"]){
                                    $response_data[$i][$t]["source"] = (string)$response_data[$i][$t]["source"] . (string)$response_data[$i][$k]["source"];
                                    $response_data[$i][$t]["description"] = (string)$response_data[$i][$t]["description"] . (string)$response_data[$i][$k]["description"];
                                    unset($response_data[$i][$k]);
                                }
                            }
                        }

                    }
                }
                $response_arch_data = new stdClass();
                $response_arch_data->retCode = "0";
                $response_arch_data->errMsg  = "";
                $response_arch_data->retBody = $response_data;


                $final_output = json_encode($response_arch_data);
                echo $final_output;

            }

            //유저가 즐겨찾기한 코드가 없을경우
            elseif ($user_code_no == ""){
                //애러는 아니고 즐겨찾기한 코드가 없을 경우이다
                // 유저의 no(index)값만 플러그인으로 response 해서 저장한다
                $response_arch_data = new stdClass();
                $response_arch_data->retCode = "1";
                $response_arch_data->errMsg  = "";
                $b = new stdClass();
                $a = array(array($b));
                $a[0][0]["user_idx"] = $user_index;
                $response_arch_data->retBody = $a;

                $final_output = json_encode($response_arch_data);
                echo $final_output;
            }


        }


        //// user의 no(index)가 없을 경우(로그인 실패)
        else if ($user_index ==""){
            $response_arch_data = new stdClass();
            $response_arch_data->retCode = "-1";
            $response_arch_data->errMsg  = "Invalid ID or PassWord";
            $b = new stdClass();
            $a = array(array($b));
            $response_arch_data->retBody = $a;

            $final_output = json_encode($response_arch_data);
            echo $final_output;
        }
        //쿼리문에 문제가 생겨 user의 no(index)가 null로 넘어올때
        else if ($user_index == null){
            $response_arch_data = new stdClass();
            $response_arch_data->retCode = "-2";
            $response_arch_data->errMsg  = "Error, Please try again (user is null)";
            $b = new stdClass();
            $a = array(array($b));
            $response_arch_data->retBody = $a;


            $final_output = json_encode($response_arch_data);
            echo $final_output;
        }
        $dbHandler->Close();
    }
    //아이디 또는 비밀번호가 공백일때("")
    else{
        $response_arch_data = new stdClass();
        $response_arch_data->retCode = "-3";
        $response_arch_data->errMsg  = "Please Insert ID or PassWord";
        $b = new stdClass();
        $a = array(array($b));
        $response_arch_data->retBody = $a;


        $final_output = json_encode($response_arch_data);
        echo $final_output;
    }
}

// 아이디 또는 비밀번호가 넘어오지못했을 경우 (isset에서 걸러진 경우)
else{
    $response_arch_data = new stdClass();
    $response_arch_data->retCode = "-4";
    $response_arch_data->errMsg  = "Error, Please try again (no post data)";
    $b = new stdClass();
    $a = array(array($b));
    $response_arch_data->retBody = $a;


    $final_output = json_encode($response_arch_data);
    echo $final_output;
}






function deleteCommentt($first_word,$last_word,$code_total){
    $place_number = 0;

    while (($place_number = strpos($code_total, $first_word, $place_number))!== false) {

        $k = strpos($code_total,$last_word,$place_number);

        $a = substr_replace($code_total,'', $place_number, $k-$place_number);
        $place_number = $place_number + strlen($first_word);
        $code_total = $a;
    }
    return $code_total;
}
