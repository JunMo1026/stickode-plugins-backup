<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once("/nginx/codecopy/backend/header/util.php");
header("Content-Type: application/json; charset=UTF-8"); // 이 코드 추가하면, 브라우저에서 json 형태로 볼 수 있음


$dbHandler = new DBHandler();
$dbHandler->setTable("code_component"); // 테이블 이름
$columns = "*"; // 가져올 컬럼
$where = "status='1'";
//$where = "status='1' && no='17'"; // where 조건
$dbHandler->setColumns($columns);
$result = $dbHandler->gets($where,"no asc", "");

$result_count = count($result["list"]);
$response_data = array();
$idx = -1;

for ($k=0; $k<$result_count; $k++) {
    $string_code = $result["list"][$k]["source"];
    $language = $result["list"][$k]["language"];

    $xml = simplexml_load_string($string_code);

    $title = $xml['title'];


    $plain_count = $xml->plain->count();
    $keycode_count =$xml->keycode->count();
    if($keycode_count !=0){
        $idx++;
    }
//plain 태그의 index 값에 맞춰 plain태그와 keycode 태그 값을 순서대로 추출함
    for ($i = 0; $i < $keycode_count; $i++) {

//    $plain_code = $xml->plain[$i];
        $key_code = $xml->keycode[$i];

        /**
         * $start_pos , $finish_pos
         * 이 두개의 변수를 주석의 시작과 끝으로 세팅해주면 된다
         * 자바일 경우 // , \n
         * xml 방식의 언어일 경우 <!-- , -->
         * DB에서 코드 가져올때, 언어도 가져와서 언어별로 if else 문 만들어주기
         *
         *
         */
        $start_pos = "//";
        $finish_pos = "\n";

        //주석 제거한 plain 태그값을 echo
//    deleteComment($start_pos,$finish_pos,$plain_code);
        //주석 제거한 keycode 값을 echo
        $source_without_comment = deleteCommentt($start_pos, $finish_pos, $key_code);
        $no_attr = $xml->keycode[$i]['no'];
        $des_attr = $xml->keycode[$i]['description'];



        $response_data[$idx][$i]["title"]= (string) $title;
        $response_data[$idx][$i]["lan"]= $language;
        $response_data[$idx][$i]["key_no"]= (string) $no_attr;
        $response_data[$idx][$i]["description"]= (string) $des_attr;
        $response_data[$idx][$i]["source"]= (string) $source_without_comment;



//        echo $title;
//        echo $language;
//        echo $no_attr;
//        echo $des_attr;
//        echo $source_without_comment;

        // 주석 제거하지 않은 plain 태그값을 echo
//    echo $plain_code;
        // 주석 제거하지 않은 keycode 태그값을 echo
//    echo $key_code;

        /**
         * 웹으로 데이터 보낼때는 주석을 제거한 코드와 제거하지 않은 코드를 모두 보내야한다.
         *
         * 이유 => 웹에서는 코드에 대한 설명을 킬 땐 주석이 있는 코드를 보여주고
         *                  코드에 대한 설명을 끌 땐 주석이 없는 코드를 보여준다.
         */

    }



}
//description 합치기 부분
// 만약 keycode 태그의 no(index)가 같다면 description 이 같아야 한다.
// DB에서는 no가 같은 keycode태그의 description 값은 공백이기에 합쳐줘야한다.
for($i = 0; $i < count($response_data); $i++){

    for ($k = 0; $k < count($response_data[$i]); $k++){
        if( $response_data[$i][$k]["description"]==""){
            $a = $response_data[$i][$k]["key_no"];
            $b = $response_data[$i][$k]["source"];
            for($t = 0; $t < $k; $t++){
                if($response_data[$i][$t]["key_no"] == $response_data[$i][$k]["key_no"]){
                    $response_data[$i][$t]["source"] = (string)$response_data[$i][$t]["source"] . (string)$response_data[$i][$k]["source"];
                    $response_data[$i][$t]["description"] = (string)$response_data[$i][$t]["description"] . (string)$response_data[$i][$k]["description"];

                    unset($response_data[$i][$k]);
                }
            }
        }

    }
}

$output = json_encode($response_data);

echo $output;

$dbHandler->Close();

function deleteCommentt($first_word,$last_word,$code_total){
    $place_number = 0;

    while (($place_number = strpos($code_total, $first_word, $place_number))!== false) {

        $k = strpos($code_total,$last_word,$place_number);

        $a = substr_replace($code_total,'', $place_number, $k-$place_number);
        $place_number = $place_number + strlen($first_word);
        $code_total = $a;
    }
    return $code_total;
}



?>