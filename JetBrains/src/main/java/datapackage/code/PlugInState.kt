package datapackage.code

/**
 * 제작자 : 신원용
 * 플러그인 DB 클래스
 */
class PlugInState {
    // 로그인한 회원이 즐겨찾기한 소스코드의 DB
    var userCustomCode =
            mutableListOf(
                    mutableListOf(
                            mutableMapOf(
                                "lan" to "JAVA",
                                "name" to "",
                                "contents" to "",
                                "description" to "")
                    )
            )

    // Stickode의 인기 TOP 100의 코드 DB
    var stickodeTopData =
            mutableListOf(
                    mutableListOf(
                            mutableMapOf(
                                "lan" to "JAVA",
                                "name" to "",
                                "contents" to "",
                                "description" to "")
                    )
            )
    }

