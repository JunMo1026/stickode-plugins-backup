package datapackage.code

/**
 * 제작자 : 신원용
 * 임시 플러그인 DB 클래스
 *
 * PlugInState.kt (플러그인 DB 클래스) 에 데이터를 직접 넣을 경우 저장이되지 않고 null이 되는 현상이 있어 임시 DB를 구축
 *
 * -코드 데이터 흐름
 * 서버 => TempDataStorage(임시 플러그인 DB) => PlugInState(플러그인 DB)
 *
 */

class TempDataStorage() {
    var tempTop100Data =
    mutableListOf(
            mutableListOf(
                    mutableMapOf(
                        "lan" to "JAVA",
                        "name" to "",
                        "contents" to "",
                        "description" to ""
                    )
            )
    )
    var tempUserCode =
        mutableListOf(
            mutableListOf(
                mutableMapOf("lan" to "JAVA",
                    "name" to "",
                    "contents" to "",
                    "description" to "")
            )
        )
}
