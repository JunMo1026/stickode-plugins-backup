package datapackage
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import datapackage.code.PlugInState

/**
 * 제작자 : 신원용
 * 플러그인 DB를 정의해주는 클래스
 * 이 클래스가 없다면 플러그인의 DB (PlugInState)를 사용할 수 없다.
 *
 * 참조 : https://www.jetbrains.org/intellij/sdk/docs/basics/persisting_state_of_components.html
 */

@State(
        //Stickode라는 이름으로 stickode-code.xml 형태로 저장한다
        name = "Stickode",
        storages = [Storage("stickode-code.xml")]
)
class PlugInStateSettings : PersistentStateComponent<PlugInState>{
    private var pluginState = PlugInState()
    override fun getState(): PlugInState? {
        return pluginState;
    }
    override fun loadState(state: PlugInState) {
        pluginState = state
    }
    companion object{
        @JvmStatic
        fun getInstance(): PersistentStateComponent<PlugInState>{
            return ServiceManager.getService(PlugInStateSettings::class.java)
        }
    }
}