package datapackage.classcollection
import cucumber.api.java.gl.E

/**
 * 제작자 : 신원용
 * 코틀린에서 Gson 데이터 선언을 위한 클래스이다.
 *
 * Tip : 코틀린에서는 Json, Gson을 바로 선언하지 못하고 data class를 선언한 후 사용해야된다.
 */

data class ResponseForm(val retCode: Int, val errMsg: String, val retBody: List<E>)
data class ResponseFormCodeData(val retCode: Int, val errMsg: String, val retBody: List<List<CodeData>>)
data class CodeData(val user_idx: String, val title: String, val lan: String, val key_no: String, val description: String, val source: String)
