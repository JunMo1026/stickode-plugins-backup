package datapackage.classcollection
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.ide.passwordSafe.PasswordSafe
/**
 * 제작자 : 신원용
 *
 * 민감 DB 모음 클래스
 * 사용자에게 암호화된 파일로 저장됌
 * 참조 :  https://www.jetbrains.org/intellij/sdk/docs/basics/persisting_sensitive_data.html?search=data
 *
 * userinfo => 유저의 ID, ID Index(no) 값 저장
 * Top100CodeAndDividedCode => Top 100 소스코드를 사용할지 말지에 대한 옵션값 저장 default : 1 / 분할된 코드를 사용할지 말지에 대한 옵션값 저장 default : 2
 * FirstTutorialsChecker => 첫 튜토리얼 실행인지 확인 (최초 실행인지 아닌지 확인으로도 쓰일 수 있을듯)
 */
class SensitiveData {
        private val credentialAttributesUserInfo = CredentialAttributes("userinfo")
        private val credentialsUserInfo = PasswordSafe.instance.get(credentialAttributesUserInfo)
        val userId = credentialsUserInfo?.userName.toString()
        var userIndex = credentialsUserInfo?.getPasswordAsString()

        private val credentialAttributesUsingTop100Source = CredentialAttributes("Top100CodeAndDividedCode")
        private val credentialsUsingTop100Source = PasswordSafe.instance.get(credentialAttributesUsingTop100Source)
        val isTopCode = credentialsUsingTop100Source?.userName.toString()
        var isDividedCode = credentialsUsingTop100Source?.getPasswordAsString()

        private val credentialAttributesTutorials = CredentialAttributes("FirstTutorialsChecker")
        private val credentialsTutorials = PasswordSafe.instance.get(credentialAttributesTutorials)
        val isFirstTutorials = credentialsTutorials?.userName.toString()
}