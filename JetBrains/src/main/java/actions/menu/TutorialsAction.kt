package actions.menu
import actions.menu.dialog.TutorialsDialogWrapper
import com.intellij.icons.AllIcons
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - Tutorials 의 Action을 지정하는 클래스
 * Tutorials 다이얼로그를 구성하는 TutorialsDialogWrapper를 호출한다.
 */
class TutorialsAction : AnAction("Tutorials","",AllIcons.Debugger.Question_badge){
    override fun actionPerformed(e: AnActionEvent) {
        val tutorialsDialog = TutorialsDialogWrapper()
        //showAndGet() 으로 Dialog 창을 호출
        if(tutorialsDialog.showAndGet()){

        }
    }

}