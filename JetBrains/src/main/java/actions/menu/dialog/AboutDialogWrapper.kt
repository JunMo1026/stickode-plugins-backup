package actions.menu.dialog
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBLabel
import com.intellij.uiDesigner.core.AbstractLayout
import com.intellij.util.ui.GridBag
import com.intellij.util.ui.JBUI
import com.sun.istack.NotNull
import java.awt.*
import javax.swing.Action
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.plaf.FontUIResource
/**
 * 제작자 : 신원용
 * AboutAction.kt 가 실행될 때의 다이얼로그를 선언하는 클래스
 *
 * 다이얼로그가 실행되기 위해서 DialogWrapper(true) 참조는 필수
 * init 을 통해 실행을 선언함 또한 필수
 *
 *  javax swing 을 통해 디자인.
 * 모르는 기능 및 method 는 javax swing 으로 검색
 */

class AboutDialogWrapper : DialogWrapper(true){
    private val panel = JPanel(GridBagLayout())
    init {
        init()
        createActions()
        title = "Stickode About"
    }

    override fun createCenterPanel(): JComponent? {
        // 다이얼로그 창의 레이아웃을 정한다.
        val gb = GridBag()
            .setDefaultInsets(Insets(0,0, AbstractLayout.DEFAULT_VGAP, AbstractLayout.DEFAULT_HGAP))
            .setDefaultWeightX(1.0) //다이얼로그 비율 1.0
            .setDefaultFill(GridBagConstraints.HORIZONTAL)  //수평 디자인 채택 (안드로이드의 LinearLayout Horizontal 과 유사)
        panel.preferredSize = Dimension(450, 200)   //다이얼로그의 크기
        panel.minimumSize = Dimension(450, 200)     //다이얼로그의 최소크기 (저 크기 이하로 창을 늘리거나 줄일 수 없음)
        //label 을 통해 텍스트를 작성
        panel.add(label("Stickode",FontUIResource("",Font.BOLD,50)), gb.nextLine().next().weightx(1.0))
        panel.add(label("Version Beta 0.2.5",FontUIResource("",Font.BOLD,30)), gb.nextLine().next().weightx(1.0))
        panel.add(label("Copyright 2020. teamnova. All rights reserved",FontUIResource("",Font.PLAIN,15)), gb.nextLine().next().weightx(1.0))
        panel.add(label("https://stickode.com | teamnovacode0410@gmail.com",FontUIResource("",Font.PLAIN,15)), gb.nextLine().next().weightx(1.0))
        return panel
    }

    // Dialogwrapper 의 버튼을 초기화해서 기본적으로 달려있는 Ok, Cancel 버튼을 없애는 fun
    @NotNull
    override fun createActions(): Array<out Action> {
        super.createDefaultActions()
        return arrayOf<Action>()
    }

    /*
    label => 텍스트를 선언해주는 fun
    파라미터
    1. text (텍스트)
    2. fontUIResource (텍스트의 특징) => ex: FontUIResource("",Font.BOLD,30) => 텍스트의 이름, 두껍게, 크기는 30
    */
    private fun label(text: String, fontUIResource: FontUIResource): JComponent{
        val label = JBLabel(text)
        label.setCopyable(true)
        label.border = JBUI.Borders.empty(0, 5, 2, 0)
        label.font = fontUIResource
        return label
    }

}