package actions.menu.dialog

import com.intellij.openapi.ui.DialogWrapper
import javax.swing.JComponent
/**
 * 제작자 : 신원용
 * StickodeAction.kt 가 실행될 때의 다이얼로그를 선언하는 클래스
 *
 * 아직 쓰이지않음, 현재는 Stickode를 누를시 Stickode 홈페이지를 브라우저로 띄워준다.
 * 추후엔 플러그인에서 다이얼로그를 통해 웹에서의 기능을 플러그인에서 사용할 수 있도록한다.
 *
 * 기획
 * 1) 소스코드를 검색해서 실시간으로 즐겨찾기 가능
 * 2) 즐겨찾기한 소스코드를 해제 가능하도록
 *
 */
class StickodeDialogWrapper  : DialogWrapper(true) {
    override fun createCenterPanel(): JComponent? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}