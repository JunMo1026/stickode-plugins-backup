package actions.menu.dialog
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.Messages
import com.intellij.ui.components.JBLabel
import com.intellij.uiDesigner.core.AbstractLayout
import com.intellij.util.ui.GridBag
import com.intellij.util.ui.JBUI
import datapackage.PlugInStateSettings
import datapackage.ServerAddress
import datapackage.code.TempDataStorage
import datapackage.classcollection.ResponseFormCodeData
import java.awt.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.border.Border

/**
 * 제작자 : 신원용
 * SignInAction.kt 가 실행될 때의 다이얼로그를 선언하는 클래스
 *
 * 다이얼로그가 실행되기 위해서 DialogWrapper(true) 참조는 필수
 * init 을 통해 실행을 선언함 또한 필수
 *
 * javax swing 을 통해 디자인.
 * 모르는 기능 및 method 는 javax swing 으로 검색
 *
 *  로그인된 경우
 *  사용자가 즐겨찾기한 코드를 가져와 플러그인 DB에 저장한다.
 *
 *  ***추후 변경 사항***
 *  1. 현재는 Top100 Data, 사용자 즐쳐찾기 코드를 가져오는 http 를 두번 진행한다.
 *  => 로그인하지 않았을 경우에 userIndex= -1 로 세팅해서 단 한번의 http로 바꿔준다.
 *  2. Top100 Data를 가져올때는 Stickode Response 구조를 따르고 있지 않다.
 *  => 1번 사항을 진행할때 함께 변경하도록 한다.
 */

class SiginInDialogWrapper : DialogWrapper(true){

    private val panel = JPanel(GridBagLayout())
    //    private val txtMode = JTextField()
    private val txtUsername = JTextField()
    private val txtPassword = JPasswordField()
    //32px 의 이미지를 아이콘으로 사용한다.
    private val stream: InputStream? = javaClass.getResourceAsStream("/icons/stickode_32.png")
    private val icon = ImageIcon(ImageIO.read(stream))

    init {
        init()
        title = "Stickode Sign In"

        //credential data 가져오기 (유저 id와 유저 idx 값)


    }

    //다이얼로그를 생성해준다
    //.add를 통해 텍스트박스 및 버튼을 생성한다.
    override fun createCenterPanel(): JComponent? {

        val gb = GridBag()
            .setDefaultInsets(Insets(0,0,AbstractLayout.DEFAULT_VGAP, AbstractLayout.DEFAULT_HGAP))
            .setDefaultWeightX(1.0)
            .setDefaultFill(GridBagConstraints.HORIZONTAL)
        panel.preferredSize = Dimension(400, 100)

//        panel.add(label("data"), gb.nextLine().next().weightx(0.2))
//        panel.add(txtMode, gb.next().weightx(0.8))

        panel.add(label("ID"), gb.nextLine().next().weightx(0.2))
        panel.add(txtUsername, gb.next().weightx(0.8))
        panel.add(label("PassWord"), gb.nextLine().next().weightx(0.2))
        panel.add(txtPassword, gb.next().weightx(0.8))
        return panel

    }

    // 이 메소드는 다이얼로그 창에서 확인 버튼을 눌렀을때 실행된다.
    override fun doOKAction() {
        val userid = txtUsername.text
        val password =txtPassword.text
        var useridx : String = "default"

        //플러그인 DB에 접근하기 위한 state 선언
        val state = PlugInStateSettings.getInstance().state
        val serverUrl = ServerAddress().ip
        // signin.php => 유저가 로그인할시, 로그인 체크 및 유저가 즐겨찾기한 코드를 가져오는 PHP 파일
        val url = URL(serverUrl+"signin.php")

        // 요청 파라미터 => user_index = userIndex (ex=> user_index = 0 )
        var reqParam = URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(userid, "UTF-8")
        reqParam += "&" + URLEncoder.encode("pwd", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8")
        with(url.openConnection() as HttpURLConnection){
            requestMethod = "POST"
            //doOutput 은 default가 false / true로 안바꿔주면 애러가 뜬다.
            doOutput = true

            //response 변수에 결과값 담아주기
            val wr = OutputStreamWriter(getOutputStream());
            wr.write(reqParam);
            wr.flush();
            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()
                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                it.close()

                // response 가 중첩된 Json형태라 Gson 사용
                val gson = GsonBuilder().create()
                val parser = JsonParser()
                // response 를 Gson 으로 파싱
                val parsed_data = parser.parse(response.toString())

                //Gson으로 파싱된 데이터를 응답포멧에 맞춰준다
                // (*코틀린에서는 Gson을 바로 쓰지 못하고 data class 선언 후 대입해서 사용해야된다)
                val responseFormat = gson.fromJson(parsed_data, ResponseFormCodeData::class.java)
                //데이터의 응답코드
                val responcode = responseFormat.retCode

//                1. ID,Password 가 틀리면(로그인실패) SignIn Fail 다이얼로그를 띄우고 비밀번호 리셋
                if(responcode != 0 && responcode != 1){
                    val errMsg = responseFormat.errMsg
                    Messages.showMessageDialog("SignIn Fail\n" +
                            errMsg, "SignIn Failure", Messages.getErrorIcon())
                 txtPassword.text=""
                }
//                  2. ID, Password 가 맞으면(로그인성공) SignIn Success 다이얼로그를 띄우고
//                  유저의 ID와 유저 index 값을 "userinfo" 라는 이름의 keychain 파일에 저장한다.
                else{
                    val retbody = responseFormat.retBody
                    //성공적으로 데이터를 가져왔을때 retCode = 0
                    if(responcode == 0){
                        /*
                        만약 유저가 즐겨찾기한 코드가 있을 경우엔 DB에 바로 넣어준다.
                        플러그인 DB에 바로 넣을 경우 저장이 안되는 현상이 있어
                        TempDataStorage (임시데이터저장소) 에 데이터를 먼저 저장한 후
                        플러그인 DB (PluginState.kt) 에 저장한다.
                        */
                        val user_code = TempDataStorage().tempUserCode
                        for(i:Int in 0..retbody.size -1){
                            user_code.add(
                                i,
                                mutableListOf(
                                    mutableMapOf(
                                        "lan" to "",
                                        "name" to "",
                                        "contents" to "",
                                        "description" to ""
                                    )
                                )
                            )
                            for(k:Int in 0..retbody[i].size-1){
                                user_code[i].add(
                                    k,
                                    mutableMapOf(
                                        "lan" to retbody[i][k].lan,
                                        "name" to retbody[i][k].title,
                                        "contents" to retbody[i][k].source,
                                        "description" to retbody[i][k].description
                                    )
                                )
                            }
                        }
                        //로그인한 유저의 no(index)값을 플러그인 민감 DB에 저장하기 위해 변수에 대입
                        useridx = retbody[0][0].user_idx
                        //TempDataStorage의 값을 플러그인 DB에 넣어준다.
                        state?.userCustomCode = user_code
                    }
                    //로그인한 유저가 즐겨찾기한 코드가 없을 경우
                    else if(responcode == 1){
                        //로그인한 유저의 no(index)값을 플러그인 민감 DB에 저장하기 위해 변수에 대입
                        useridx = retbody[0][0].user_idx

                    }

                    //다이얼로그를 닫고 로그인이 성공했음을 알리는 팝업창을 띄운다.
                    doCancelAction()
                    Messages.showMessageDialog("Sign In Success", "Sign In", icon)
                    // 플러그인 민감 DB 에 사용자 ID와 사용자 no(index)를 저장한다.
                val credentialAttributes = CredentialAttributes("userinfo")
                val credentials =  Credentials(userid,useridx)
                PasswordSafe.instance.set(credentialAttributes, credentials)
                }
            }
        }
    }

    //label => 텍스트를 선언해주는 fun
    private fun label(text: String): JComponent{
        val label = JBLabel(text)
        label.border = JBUI.Borders.empty(0, 5, 2, 0)
        return label

    }
}