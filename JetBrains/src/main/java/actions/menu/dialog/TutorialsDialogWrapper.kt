package actions.menu.dialog

import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.ui.DialogWrapper
import com.sun.istack.NotNull
import datapackage.classcollection.SensitiveData
import java.awt.*
import javax.swing.*
import javax.swing.border.LineBorder

/**
 * 제작자 : 신원용
 * TutorialsAction.kt 가 실행될 때의 다이얼로그를 선언하는 클래스
 *
 * 플러그인의 사용방법(튜토리얼)을 보여주는 Dialog.
 *
 * 1. PreLoading.kt (툴이 실행될때 최초로 실행되는 플러그인 액션) 가 실행될 때
 * 플러그인을 설치하고 최초로 실행할 때 튜토리얼을 보여준다.
 * 2. Stickode 메뉴에서 Tutorials 를 클릭했을때 보여준다.
 *
 */
class TutorialsDialogWrapper : DialogWrapper(true){
    private val panel = JPanel(GridBagLayout())
    private val tutorialGifImage = JLabel()
    private val tutorialText = JLabel()
    private val previousButton = JButton()
    private val nextButton = JButton()
    private var pageNumber = 0
    init {
        pageNumber = 0
        init()
        createActions()
        title = "Stickode Tutorials"
    }

    override fun createCenterPanel(): JComponent? {

        val gb = GridBagLayout()
        gb.columnWidths = intArrayOf(10, 35, 105, 410, 105, 35, 10, 0)
        gb.rowHeights = intArrayOf(0, 385, 28, 15, 0, 10, 0)
        panel.layout = gb

        panel.minimumSize = Dimension(2147483647, 2147483647)
        panel.preferredSize = Dimension(725, 485)




        tutorialGifImage.icon = ImageIcon(javaClass.getResource("/icons/only_code_version.gif"))
        tutorialGifImage.horizontalAlignment = SwingConstants.CENTER
        tutorialGifImage.preferredSize = Dimension(700,377)
        tutorialGifImage.border = LineBorder.createBlackLineBorder()
        panel.add(tutorialGifImage, GridBagConstraints(
            1, 1, 5, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            Insets(0, 0, 5, 5), 0, 0
        ))

        tutorialText.text = "Write the title of the source code you want to use, The result is fantastic"
        tutorialText.horizontalAlignment = SwingConstants.CENTER
        tutorialText.font = tutorialText.font.deriveFont(tutorialText.font.style or Font.BOLD, tutorialText.font.size + 2f)
        panel.add(tutorialText, GridBagConstraints(
                2, 2, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            Insets(0, 0, 5, 5), 0, 0
        ))

        previousButton.text = "Previous"
        previousButton.font = previousButton.font.deriveFont(previousButton.font.style or Font.BOLD)
        panel.add(previousButton, GridBagConstraints(
            2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            Insets(0, 0, 5, 5), 0, 0
        ))
        if (pageNumber == 0){
            previousButton.isEnabled = false
        }

        nextButton.text = "Next"
        nextButton.font = nextButton.font.deriveFont(nextButton.font.style or Font.BOLD)
        panel.add(nextButton,GridBagConstraints(
            4, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            Insets(0, 0, 5, 5), 0, 0
        ))

        /*  이전 버튼 클릭 액션
            첫 튜토리얼 화면에서는 비활성화

        */
        previousButton.addActionListener(){
            pageNumber = 0
            nextButton.text = "Next"
            previousButton.isEnabled = false
            tutorialText.text = "Write the title of the source code you want to use, The result is fantastic"
            tutorialGifImage.icon = ImageIcon(javaClass.getResource("/icons/only_code_version.gif"))
        }
        /*  다음 버튼 클릭 액션
            마지막 튜토리얼 화면에서는 Finish로 변경

        */
        nextButton.addActionListener(){
            if(pageNumber == 0){
                pageNumber = 1
                previousButton.isEnabled = true
                nextButton.text = "Finish"
                tutorialText.text = "Try more features in the Stickode menu"
                tutorialGifImage.icon = ImageIcon(javaClass.getResource("/icons/only_option.gif"))
            }
            else if(pageNumber == 1){
                doCancelAction()
            }

        }

        return panel
    }

    // 튜토리얼을 종료할때 민감 DB의 튜토리얼 최초실행 값을 0으로 해준다. => 플러그인 최초 실행이후에는 튜토리얼이 실행되지 않는다.
    override fun doCancelAction() {
        val credentialAttributesTutorials = CredentialAttributes("FirstTutorialsChecker")
        val credentials = Credentials("0", "NotUsing")
        PasswordSafe.instance.set(credentialAttributesTutorials, credentials)
        super.doCancelAction()
    }

    //기본적으로 Dialog에 있는 OK, Cancel 버튼 없애기
    @NotNull
    override fun createActions(): Array<out Action> {
        super.createDefaultActions()
        return arrayOf<Action>()
    }
}