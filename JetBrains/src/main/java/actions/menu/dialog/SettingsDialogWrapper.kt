package actions.menu.dialog

import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.icons.AllIcons
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.ui.ComboBox
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.components.JBTabbedPane
import datapackage.classcollection.SensitiveData
import java.awt.*
import javax.swing.*

class SettingsDialogWrapper : DialogWrapper(true){

    private val tab = JBTabbedPane()
    private val panelSettings = JPanel(GridBagLayout())
    private val usingTopCode =  JCheckBox()
    private val usingDividedCode =  JCheckBox()
    private val panelBookmark =  JPanel(GridBagLayout())

    private val label1 =  JLabel()
    private val comboBox1 =  ComboBox<String?>()
    private val textFieldSearch =  JTextField()
    private val buttonSearch =  JButton()
    private val scrollPaneSearch =  JBScrollPane()
    private val panelSearch =  JPanel(GridBagLayout())

    private val label2 =  JLabel()
    private val scrollPaneBookmark =  JBScrollPane()

    private val isUsingTopCode = SensitiveData().isTopCode
    private val isUsingDividedCode = SensitiveData().isDividedCode

    private val panel4 =  JPanel(GridBagLayout())
    init {
        init()
        createActions()
        title = "Stickode Settings"
        // 사용자가 Top100Code 를 쓴다면 CheckBox = true
        // 사용자가 Top100Code 를 쓰지 않는다면 CheckBox = false
        if(isUsingTopCode=="0"){
            usingTopCode.isSelected = false
        }else{
            usingTopCode.isSelected = true
        }
        // 사용자가 분할된 코드를 쓴다면 CheckBox = true
        // 사용자가 분할된 코드를 쓰지 않는다면 CheckBox = false
        if(isUsingDividedCode=="0"){
            usingDividedCode.isSelected = false
        }else{
            usingDividedCode.isSelected = true
        }

    }
    override fun createCenterPanel(): JComponent? {
        tab.tabPlacement = SwingConstants.LEFT
        tab.preferredSize = Dimension(800, 400)

        panelSettings.font = panelSettings.font.deriveFont(panelSettings.font.style or Font.BOLD)
        val gb1 = GridBagLayout()
        gb1.columnWidths = intArrayOf(20, 0, 0)
        gb1.rowHeights = intArrayOf(25, 0, 0, 0)
        panelSettings.layout = gb1

        usingTopCode.text = "Using Top 100 source code"
        panelSettings.add(
            usingTopCode, GridBagConstraints(
                1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 0), 0, 0
            )
        )

        usingDividedCode.text = "Using divided source code"
        panelSettings.add(
            usingDividedCode, GridBagConstraints(
                1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 0, 0), 0, 0
            )
        )
        tab.addTab("Settings", panelSettings)

        val gb2 = GridBagLayout()
        gb2.columnWidths = intArrayOf(20, 0, 459, 45, 15, 0)
        gb2.rowHeights = intArrayOf(15, 20, 0, 128, 180, 0)
        panelSearch.layout = gb2

        label1.text = "Source Code Search"
        label1.horizontalAlignment = SwingConstants.CENTER
        label1.font = label1.font.deriveFont(label1.font.style or Font.BOLD)
        panelSearch.add(
            label1, GridBagConstraints(
                1, 1, 3, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )
        //---- comboBox1 ----
        comboBox1.model = DefaultComboBoxModel<String>(
            arrayOf(
                "All Languages",
                "Android(Java)",
                "Android(Kotlin)",
                "Java",
                "Kotlin",
                "JavaScript",
                "PHP"
            )
        )
        panelSearch.add(
            comboBox1, GridBagConstraints(
                1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )
        panelSearch.add(
            textFieldSearch, GridBagConstraints(
                2, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )

        buttonSearch.icon = AllIcons.Actions.Search
        panelSearch.add(
            buttonSearch, GridBagConstraints(
                3, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )
        panelSearch.add(
            scrollPaneSearch, GridBagConstraints(
                1, 3, 3, 2, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 0, 5), 0, 0
            )
        )
//        tab.addTab("Searching", panelSearch)
















        val gb3 = GridBagLayout()
        gb3.columnWidths = intArrayOf(20, 705, 15, 0)
        gb3.rowHeights = intArrayOf(15, 20, 355, 15, 0)
        panelBookmark.layout = gb3
        label2.text = "Your Bookmark Code"
        label2.font = label2.font.deriveFont(label2.font.style or Font.BOLD)
        label2.horizontalAlignment = SwingConstants.CENTER
        panelBookmark.add(
            label2, GridBagConstraints(
                1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )
        scrollPaneBookmark.background = Color(76, 80, 82)
        scrollPaneBookmark.horizontalScrollBarPolicy = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
        scrollPaneBookmark.verticalScrollBarPolicy = ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS
        panelBookmark.add(
            scrollPaneBookmark, GridBagConstraints(
                1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                Insets(0, 0, 5, 5), 0, 0
            )
        )
//        tab.addTab("Bookmarks", panelBookmark)



//        val gb4 = GridBagLayout()
//        gb4.columnWidths = intArrayOf(30, 0)
//        gb4.rowHeights = intArrayOf(20, 20, 0)
//        panel4.layout = gb4



        return tab
    }

    // 다이얼로그에서 확인 버튼을 눌렀을 때 실행되는 fun
    override fun doOKAction() {
        // 플러그인 민감 DB Top100 코드 사용 여부를 저장
        // 체크박스가 체크됐다면 1, 체크를 해제했다면 0
        val credentialAttributes = CredentialAttributes("Top100CodeAndDividedCode")
        if(usingTopCode.isSelected){
            if(usingDividedCode.isSelected){
                val credentials =  Credentials("1","1")
                PasswordSafe.instance.set(credentialAttributes, credentials)
            }else if(!usingDividedCode.isSelected){
                val credentials =  Credentials("1","0")
                PasswordSafe.instance.set(credentialAttributes, credentials)
            }
        }else if(!usingTopCode.isSelected){
            if(usingDividedCode.isSelected){
                val credentials =  Credentials("0","1")
                PasswordSafe.instance.set(credentialAttributes, credentials)
            }else if(!usingDividedCode.isSelected){
                val credentials =  Credentials("0","0")
                PasswordSafe.instance.set(credentialAttributes, credentials)
            }
        }
        //다이얼로그를 닫음
        doCancelAction()
    }


}