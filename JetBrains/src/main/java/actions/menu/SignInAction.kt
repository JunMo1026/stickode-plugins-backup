package actions.menu
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import datapackage.classcollection.SensitiveData
import actions.menu.dialog.SiginInDialogWrapper

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - SignIn 의 Action을 지정하는 클래스
 * SignIn (로그인)을 실행시킨다.
 *
 * 로그인과 동시에 사용자에 대한 정보를 가져와 저장한다.
 * 1. 사용자 ID        =>  (플러그인 민감 DB)
 * 2. 사용자 no(index) =>  (플러그인 민감 DB)
 * 3. 사용자 ID에 즐겨찾기되어 있는 소스코드  =>  (플러그인 DB)
 *
 * 로그인이 되지 않은 상태에서만 보여지는 버튼
 * 로그인이 된 상태에서는 Sign Out(로그아웃) 버튼이 보여진다.
 */

class SignInAction : AnAction("Sign In") {

    override fun actionPerformed(e: AnActionEvent) {
        //showAndGet() 으로 Dialog 창을 호출
        val signInDialog = SiginInDialogWrapper()
        if(signInDialog.showAndGet()){
        }
    }

    /*
    update 는 사용자가 상단메뉴바 Stickode를 누를때 실행된다.
    로그인이 되어있다면 Sign In을 감추고, 로그인이 되어있지 않다면 Sign In을 보여준다.
    */
    override fun update(event: AnActionEvent) {
        super.update(event)
        //민감 데이터 DB 에 저장되어있는 사용자의 no(index)값 불러오기
        val userIndex = SensitiveData().userIndex

        /*
        플러그인 첫 설치시에는 userIndex= null 상태,
        로그아웃을 통해 초기화 시켜줄땐 userIndex="default" 상태이다.
        */
        if(userIndex == "default" || userIndex == null){
            event.presentation.isVisible = true
        }else{
            event.presentation.isVisible = false
        }
    }



}
