package actions.menu
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import java.awt.Desktop
import java.net.URI

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - SignUp 의 Action을 지정하는 클래스
 * Stickode 회원가입 페이지를 브라우저를 통해 띄워준다.
 */
class SignUpAction : AnAction("Sign Up"){
    override fun actionPerformed(e: AnActionEvent) {
        //웹페이지 띄워주기 => 회원가입 창으로 이동
        Desktop.getDesktop().browse( URI("https://stickode.com/signup.html"));
    }

}