package actions.menu
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.ui.Messages
import datapackage.classcollection.SensitiveData
import datapackage.PlugInStateSettings
import datapackage.code.TempDataStorage

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - SignOut 의 Action을 지정하는 클래스
 * SignOut (로그아웃)을 실행시킨다.
 *
 * 로그아웃과 동시에 사용자에 대한 정보를 초기화시켜준다.
 * 1. 사용자 ID        =>  (플러그인 민감 DB)
 * 2. 사용자 no(index) =>  (플러그인 민감 DB)
 * 3. 사용자 ID에 즐겨찾기되어 있는 소스코드  =>  (플러그인 DB)
 *
 * 로그인이 된 상태에서만 보여지는 버튼
 * 로그인이 되지 않은 상태에서는 Sign In(로그인) 버튼이 보여진다.
 *
 */

class SignOutAction : AnAction("Sign Out"){
    override fun actionPerformed(e: AnActionEvent) {

        //플러그인 DB에 접근하기 위한 state 선언
        val state = PlugInStateSettings.getInstance().state

        //플러그인 민감 DB에 저장되어있는 사용자 ID, no(index)를 초기화시켜준다.
        val credentialAttributes = CredentialAttributes("userinfo")
        val credentials =  Credentials("","default")  //입력한 ID와 response로부터 idx 값 넣기
        PasswordSafe.instance.set(credentialAttributes, credentials)

        //플러그인 DB에 있는 사용자 즐겨찾기 소스코드를 초기화시켜준다.
        val user_code = TempDataStorage().tempUserCode
        user_code.add(0,mutableListOf(mutableMapOf("lan" to "", "name" to "", "contents" to "", "description" to "")))
        state?.userCustomCode = user_code

        // 로그아웃 성공 팝업창
        Messages.showMessageDialog("Sign Out Success", "Sign Out", Messages.getInformationIcon())

    }

    /*
    update 는 사용자가 상단메뉴바 Stickode를 누를때 실행된다.
    로그인이 되어있다면 Sign Out을 보여주고, 로그인이 되어있지 않다면 Sign Out을 감춘다.
    */
    override fun update(event: AnActionEvent) {
        super.update(event)
        //민감 데이터 DB 에 저장되어있는 사용자의 no(index)값 불러오기
        val userIndex = SensitiveData().userIndex

        /*
        플러그인 첫 설치시에는 userIndex= null 상태,
        로그아웃을 통해 초기화 시켜줄땐 userIndex="default" 상태이다.
        */
        if(userIndex == "default" || userIndex == null){
            event.presentation.isVisible = false
        }else{
            event.presentation.isVisible = true
        }
    }



}