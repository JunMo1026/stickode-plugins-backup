package actions.menu

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.util.IconLoader
import java.awt.Desktop
import java.net.URI

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - Stickode 의 Action을 지정하는 클래스
 * Stickode 공식 웹페이지를 브라우저를 통해 띄워준다. (임시)
 *
 * 추후엔 플러그인에서 다이얼로그를 통해 웹에서의 기능을 플러그인에서 사용할 수 있도록한다.
 *
 * 기획
 * StickodeDialogWrapper.kt 참조
 */

class StickodeAction : AnAction("Stickode","", IconLoader.findIcon("/icons/stickode_14.png")){
    override fun actionPerformed(e: AnActionEvent) {

//        웹페이지 띄워주기 => 임시용
        Desktop.getDesktop().browse( URI("https://www.stickode.com/"));

        // 지금은 웹페이지를 띄워주지만 추후엔 플러그인에서 웹의 역할을 할 수 있도록 한다.
//        val logindialog = StickodeDialogWrapper()
//        if(logindialog.showAndGet()){
//        }
    }

}