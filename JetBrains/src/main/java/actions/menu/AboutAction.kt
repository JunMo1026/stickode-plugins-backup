package actions.menu
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import actions.menu.dialog.AboutDialogWrapper

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - About 의 Action을 지정하는 클래스
 * About 다이얼로그를 구성하는 AboutDialogWrapper를 호출한다.
 */
class AboutAction : AnAction("About"){
    override fun actionPerformed(e: AnActionEvent) {

        val aboutDialog = AboutDialogWrapper()
        //showAndGet() 으로 Dialog 창을 호출
        if(aboutDialog.showAndGet()){

        }

    }

}