package actions.menu
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import actions.menu.dialog.SettingsDialogWrapper
import com.intellij.icons.AllIcons

/**
 * 제작자 : 신원용
 *
 * 상단 메뉴바 Stickode - Settings 의 Action을 지정하는 클래스
 * Settings 다이얼로그를 구성하는 SettingsDialogWrapper를 호출한다.
 */

class SettingsAction : AnAction("Settings","", AllIcons.General.GearPlain){
    override fun actionPerformed(e: AnActionEvent) {
        val settingsDialog = SettingsDialogWrapper()
        //showAndGet() 으로 Dialog 창을 호출
        if(settingsDialog.showAndGet()){

        }
    }

}