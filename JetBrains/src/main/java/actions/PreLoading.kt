package actions
import actions.menu.UpdateCodeAction
import actions.menu.dialog.TutorialsDialogWrapper
import com.google.gson.*
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.application.PreloadingActivity
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.ui.Messages
import datapackage.classcollection.SensitiveData
import datapackage.PlugInStateSettings
import datapackage.ServerAddress
import datapackage.code.TempDataStorage
import datapackage.classcollection.CodeData
import datapackage.classcollection.ResponseFormCodeData
import org.jdesktop.swingx.action.ActionManager
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.swing.SwingUtilities

/**
 * 제작자 : 신원용
 *
 * 플러그인이 실행될 때 (툴이 실행될때 플러그인도 실행)
 * 먼저 실행되는(preload)되는 클래스
 *
 *
 * UpdateCodeAction 과 동일한 역할을 함 (코드 또한 동일)
 * 1. 로그인된 경우
 *  Stickode의 Top100 Data와 사용자가 즐겨찾기한 코드를 가져와 플러그인 DB에 저장한다.
 *
 * 2. 로그인이 되지 않은 경우
 *  사용자가 즐겨찾기한 코드를 가져와  플러그인 DB에 저장한다.
 *
 *  ***추후 변경 사항***
 *  UpdateCodeAction.kt 참조
 */

class PreLoading : PreloadingActivity() {
    override fun preload(indicator: ProgressIndicator) {

        // 플러그인 민감 DB 에 저장되어있는 최초 튜토리얼 실행여부 가져오기.
        // 플러그인 첫 실행시에는 민감DB가 null 상태이기때문에 최초 실행이 맞다는 "1" 로 세팅해준다.
        val isFirstTutorials = SensitiveData().isFirstTutorials
        val credentialAttributesTutorials = CredentialAttributes("FirstTutorialsChecker")
        if(isFirstTutorials != "1"){
            val credentials = Credentials("1", "NotUsing")
            PasswordSafe.instance.set(credentialAttributesTutorials, credentials)
        }
        //플러그인 최초 실행시에만 튜토리얼 창 실행
        if(isFirstTutorials != "0"){
            SwingUtilities.invokeLater(kotlinx.coroutines.Runnable {
                val tutorialsDialog = TutorialsDialogWrapper()
                //showAndGet() 으로 Dialog 창을 호출
                if(tutorialsDialog.showAndGet()){
                }
            })
        }

        //플러그인 민감 DB 에 저장되어있는 사용자의 no(index)값 불러오기 (Stickode DB 에서 사용자 no 값으로 즐겨찾기한 소스코드의 no(index)를 가져온다)
        val userIndex = SensitiveData().userIndex
        //플러그인 DB에 접근하기 위한 state 선언
        val state = PlugInStateSettings.getInstance().state

        //플러그인 실행시, Top100Code 사용 및 분할된 소스코드 사용하기 setting 이 되어 있지 않다면 default로 설정해주기
        //default: Top100Code ="1"(사용O), DividedCode = "0"(사용X)
        var isUsingTopCode = SensitiveData().isTopCode
        var isUsingDividedCode = SensitiveData().isDividedCode
        val credentialAttributes = CredentialAttributes("Top100CodeAndDividedCode")
        if(isUsingTopCode != "1" && isUsingTopCode != "0"){
            isUsingDividedCode = SensitiveData().isDividedCode
            val credentials =  Credentials("1",isUsingDividedCode)
            PasswordSafe.instance.set(credentialAttributes, credentials)
        }
        if(isUsingDividedCode != "1" && isUsingDividedCode != "0"){
            isUsingTopCode = SensitiveData().isTopCode
            val credentials =  Credentials(isUsingTopCode,"0")
            PasswordSafe.instance.set(credentialAttributes, credentials)
        }


        val serverUrl = ServerAddress().ip
        // plugins_by_user_no.php => 유저가 즐겨찾기한 소스코드를 가져오는 PHP 파일
        val url = URL(serverUrl+"code_usercustom.php")
        if(userIndex!=null) {
            val reqParam = URLEncoder.encode("user_index", "UTF-8") + "=" + URLEncoder.encode(userIndex, "UTF-8")
            with(url.openConnection() as HttpURLConnection) {
                requestMethod = "POST"
                //doOutput 은 default가 false / true로 안바꿔주면 애러가 뜬다.
                doOutput = true

                //response 변수에 결과값 담아주기
                val wr = OutputStreamWriter(getOutputStream());
                wr.write(reqParam);
                wr.flush();
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()
                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    it.close()

                    // response 가 중첩된 Json형태라 Gson 사용
                    val gson = GsonBuilder().create()
                    val parser = JsonParser()
                    // response 를 Gson 으로 파싱
                    val parsedData = parser.parse(response.toString())

                    //Gson으로 파싱된 데이터를 응답포멧에 맞춰준다
                    // (*코틀린에서는 Gson을 바로 쓰지 못하고 data class 선언 후 대입해서 사용해야된다)
                    val responseFormat = gson.fromJson(parsedData, ResponseFormCodeData::class.java)
                    //데이터의 응답코드
                    val responCode = responseFormat.retCode

//                  응답코드가 0이 아닌때 (데이터를 가져올때 문제가 생겼을 경우)
                    // 로그인 안할 상태로 Update할 상황을 추가해줘야함.
//                if(responcode != 0){
//                    val errMsg = responseFormat.errMsg
//                    Messages.showMessageDialog("Update Fail (loading User Code Data)\n" +
//                            errMsg, "Update Failure", Messages.getErrorIcon())
//
//                }

                    //성공적으로 데이터를 가져왔을때 retCode = 0
                    if (responCode == 0) {
                        val retbody = responseFormat.retBody

                        /*
                        플러그인 DB에 바로 넣을 경우 저장이 안되는 현상이 있어
                        TempDataStorage (임시데이터저장소) 에 데이터를 먼저 저장한 후
                        플러그인 DB (PluginState.kt) 에 저장한다.
                        */
                        val user_code = TempDataStorage().tempUserCode
                        for (i: Int in 0..retbody.size - 1) {
                            user_code.add(
                                i,
                                mutableListOf(
                                    mutableMapOf(
                                        "lan" to "",
                                        "name" to "",
                                        "contents" to "",
                                        "description" to ""
                                    )
                                )
                            )
                            for (k: Int in 0..retbody[i].size - 1) {
                                user_code[i].add(
                                    k,
                                    mutableMapOf(
                                        "lan" to retbody[i][k].lan,
                                        "name" to retbody[i][k].title,
                                        "contents" to retbody[i][k].source,
                                        "description" to retbody[i][k].description
                                    )
                                )
                            }
                        }
                        //TempDataStorage의 값을 플러그인 DB에 넣어준다.
                        state?.userCustomCode = user_code
                    }
                }
            }
        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // plugins.php => Stickode Top100 Data 를 가져오는 PHP 파일
        val url2 = serverUrl + "code_top100.php"
        val connection = URL(url2).openConnection() as HttpURLConnection
        try {
            // data 에 서버로부터 받은 response 값을 저장
            val data = connection.inputStream.bufferedReader().readText()

            // data 가 중첩된 Json형태라 Gson 사용
            val gson = GsonBuilder().create()
            val parser = JsonParser()

            // data 를 Gson 으로 파싱
            val parsedData2 = parser.parse(data)

            /*
               플러그인 DB에 바로 넣을 경우 저장이 안되는 현상이 있어
               TempDataStorage (임시데이터저장소) 에 데이터를 먼저 저장한 후
               플러그인 DB (PluginState.kt) 에 저장한다.
             */
            val topData = TempDataStorage().tempTop100Data
            for(i:Int in 0..parsedData2.asJsonArray.size()-1){
                topData.add(
                    i,
                    mutableListOf(
                        mutableMapOf(
                            "lan" to "",
                            "name" to "",
                            "contents" to "",
                            "description" to ""
                        )
                    )
                )
                for(k:Int in 0..parsedData2.asJsonArray.get(i).asJsonArray.size()-1){
                    val rootObj = parser.parse(data).asJsonArray.get(i).asJsonArray.get(k)
                    val datas =  gson.fromJson(rootObj, CodeData::class.java)
                    topData[i].add(
                        k,
                        mutableMapOf(
                            "lan" to datas.lan,
                            "name" to datas.title,
                            "contents" to datas.source,
                            "description" to datas.description)
                    )

                }
            }
            //TempDataStorage의 값을 플러그인 DB에 넣어준다.
            state?.stickodeTopData = topData
            //참고 : http://blog.naver.com/PostView.nhn?blogId=cosmosjs&logNo=221455368269&categoryNo=87&parentCategoryNo=0
        }
        catch (e: Exception){
            SwingUtilities.invokeLater(kotlinx.coroutines.Runnable {
                Messages.showMessageDialog(
                    "CodeCopy Update Failure\n" +
                            "Please use in the Internet environment\n" +
                            "Press CodeCopy-Update"
                    , "CodeCopy Update Failure", Messages.getErrorIcon())
            })
        }
        finally {
            connection.disconnect()
        }
    }
}
