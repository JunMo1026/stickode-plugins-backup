package autocompletion.languages;
import autocompletion.common.method.StickodeCompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import com.sun.istack.NotNull;
/**
 * 제작자 : 신원용
 * JAVA 자동완성을 선언하는 클래스
 *
 * 언어별 각 클래스를 plugin.xml 에서 선언해줘야하므로
 * 언어가 추가될때마다 언어별 클래스를 생성해줘야한다.
 *
 * CompletionContributor 참조 필수
 */

public class PHP_CompletionContributor extends com.intellij.codeInsight.completion.CompletionContributor {
    public PHP_CompletionContributor(){
        extend(CompletionType.BASIC,
                PlatformPatterns.psiElement(),
                new CompletionProvider<CompletionParameters>() {
                    public void addCompletions(@NotNull CompletionParameters parameters,
                                               ProcessingContext context,
                                               @org.jetbrains.annotations.NotNull @NotNull CompletionResultSet result) {
                        //자동완성을 시켜주는 메소드를 선언
                        // 언어와 addCompletions 의 파라미터들을 보낸다.
                        StickodeCompletionContributor SCC = new StickodeCompletionContributor();
                        SCC.StickodeCompletionContributor("php", parameters, context, result);
                    }
                }
        );
    }
}
