package autocompletion.common.method;
import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.application.ApplicationInfo;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import datapackage.ServerAddress;
import datapackage.classcollection.SensitiveData;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jetbrains.annotations.NotNull;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 제작자 : 신원용
 * Top100 소스 코드의 자동완성의 레이아웃을 만들어주는 클래스
 *
 * 정식 출시전에 사용자 로그 구현p
 *
 */

 class AddNewLookupElementBasicOffer {
    void BasicOfferAddNewLookupElement(CompletionResultSet resultSet, String title, String contents, String description, String isdivided, String language) {

        ServerAddress SA = new ServerAddress();
        String ip = SA.getIp();
        //아이콘 이미지 선언
        InputStream stream = getClass().getResourceAsStream("/icons/stickode_16.png");
        if(!title.equals("")) {
            try {
                ImageIcon icon = new ImageIcon(ImageIO.read(stream));
                LookupElement element = LookupElementBuilder
                        .create(title + description)
                        // 좌측에 아이콘 이미지 추가
                        .withIcon(icon)


                        /*
                         자동완성 옵션의 텍스트 색깔을 설정 (autocompletion.common.method 패키지의 ColorCollection을 통해 지정)
                         텍스트 색깔을 지정할 경우, 테마에 따른 색깔 변화를 못준다.
                         Top 100 데이터의 경우엔 기본적으로 적용 테마(Darcula, intellij 등)에 따른 폰트 색을 따르고, 이미지로 구분하도록 한다.
                         사용자 즐겨찾기 및 커스텀 코드 데이터의 경우에는 테마에 크게 영향받지않는 green 색으로 설정
                         */
//                    .withItemTextForeground(ColorCollection.BLACK)

                        // 우측 끝에 달리는 텍스트, 현재는 코드에 대한 설명을 넣어둠 (ex: varialbe / full code)
                        .withTypeText(isdivided)


                        //글자 두껍게
                        .bold()

                        //아이콘 우측으로 정렬
                        .withTypeIconRightAligned(true)

                        // 자동완성 성공시 실행되는 핸들러 ==> 로그 쌓기 구현 진행하면됌
                        .withInsertHandler(new InsertHandler<LookupElement>() {
                            @Override
                            public void handleInsert(@NotNull InsertionContext insertionContext, @NotNull LookupElement lookupElement) {
                                Editor editor = insertionContext.getEditor();
                                Document document = editor.getDocument();
                                int startOffset = insertionContext.getStartOffset();
                                int taleOffset = insertionContext.getTailOffset();
                                // 제목으로 자동완성옵션을 찾지만, 자동완성은 소스코드내용으로 바꾸기
                                document.replaceString(startOffset, taleOffset, contents);

                                /*
                                사용자 로그 쌓기
                                1. 사용자 ID
                                2. 사용자가 Top100Code를 사용했는지, Custom 코드를 사용했는지
                                3. 소스코드 제목
                                4. 사용 언어
                                5. 사용한 툴
                                6. 사용한 툴의 버전
                                 */
                                SensitiveData gcf = new SensitiveData();
                                String user_index_for_log = gcf.getUserIndex();
                                if (user_index_for_log == null || user_index_for_log.equals("default")) {
                                    //로그인 하지 않는 사용자가 사용할땐 로그에 보낼 logUser = -1
                                    user_index_for_log = "-1";
                                }
                                String editor_name = ApplicationInfo.getInstance().getVersionName();
                                String editor_version = ApplicationInfo.getInstance().getFullVersion();

                                String logData ="{" +
                                                "\"logPage\":\"Plugins\"," +
                                                "\"logType\":\"Top100\"," +
                                                "\"logUser\":\""+user_index_for_log+"\"," +
                                                "\"code_title\":\""+title+"\"," +
                                                "\"language\":\""+language+"\"," +
                                                "\"tool_title\":\""+editor_name+"\"," +
                                                "\"tool_version\":\""+editor_version+"\"" +
                                                "}";

                                HttpClient httpclient = HttpClients.createDefault();
                                HttpPost httppost = new HttpPost(ip+"log.php");

                                List<NameValuePair> params = new ArrayList<NameValuePair>(2);
                                params.add(new BasicNameValuePair("codeCompletion", logData));
                                try {
                                    httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                                    HttpResponse response = httpclient.execute(httppost);
                                    HttpEntity entity = response.getEntity();
                                    if (entity != null) {
                                        try (InputStream instream = entity.getContent()) {
                                            System.out.println(instream.toString());
                                        }
                                    }
                                }
                                catch (IOException e) {
                                        e.printStackTrace();
                                }
                            }
                        });
                resultSet.addElement(element);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
