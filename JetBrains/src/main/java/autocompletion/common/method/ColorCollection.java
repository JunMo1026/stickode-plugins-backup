package autocompletion.common.method;
import com.intellij.ui.DarculaColors;
import com.intellij.ui.JBColor;
import java.awt.*;
/**
 * 제작자 : 신원용
 * 플러그인 색깔 모음 클래스
 *
 * 사용하다보니 JBColor에서 기본제공하는 색깔의 종류가 적어서 만듦
 * 원하는 색이 있다면 RGB 값을 통해 새롭게 선언해서 사용
 */

public class ColorCollection {


    
    public static final Color cyan = new JBColor(Color.cyan, new Color(42, 193, 188));
    public static final Color CYAN = cyan;

    public static final Color red = new JBColor(Color.red, new Color(255, 0, 0));
    public static final Color RED = red;

    public static final Color blue = new JBColor(Color.blue, DarculaColors.BLUE);
    public static final Color BLUE = blue;
    public static final Color dark_blue = new JBColor(Color.blue, new Color(30, 4, 120));
    public static final Color DARK_BLUE = dark_blue;

    public static final Color white = new JBColor(Color.white, new Color(255, 255, 255));
    public static final Color WHITE = white;

    public static final Color black = new JBColor(Color.black, new Color(0, 0, 0));
    public static final Color BLACK = black;

    public static final Color orange = new JBColor(Color.orange, new Color(159, 107, 0));
    public static final Color ORANGE = orange;

    public static final Color yellow = new JBColor(Color.yellow, new Color(138, 138, 0));
    public static final Color YELLOW = yellow;

    public static final Color green = new JBColor(Color.green, new Color(84, 157, 89));
    public static final Color GREEN = green;

    public static final Color magenta = new JBColor(Color.magenta, new Color(151, 118, 169));
    public static final Color MAGENTA = magenta;

}
