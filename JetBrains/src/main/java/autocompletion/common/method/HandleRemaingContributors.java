package autocompletion.common.method;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElement;
/**
 * 제작자 : 신원용
 *
 * 사용자가 자판을 입력할때마다 해당 글자에 일치하는 자동완성이 있는지를 찾는 핸들러 클래스
 *
 * 사용자가 입력한 글자에 일치하는 소스코드가 있다면 자동완성 옵션을 만들어 보여준다.
 */

class HandleRemaingContributors {
    void handleremainingcontributors(@org.jetbrains.annotations.NotNull CompletionParameters parameters,
                                     @org.jetbrains.annotations.NotNull CompletionResultSet result) {
        result.runRemainingContributors(parameters, completionResult -> {
            LookupElement lookupElement = completionResult.getLookupElement();
            result.addElement(lookupElement);
        });
    }
}
