package autocompletion.common.method;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.util.ProcessingContext;
import com.sun.istack.NotNull;
import datapackage.classcollection.SensitiveData;
import datapackage.PlugInStateSettings;
import datapackage.code.PlugInState;
/**
 * 제작자 : 신원용
 * 자동완성시 소스코드의
 * 이름, 설명, 소스코드, 넣어줄 아이콘
 * 을 선언하는 클래스
 *
 * 유저가 타이핑을 할때마다 이 클래스가 실행되며 자동완성을 실행한다.
 *
 *
 */
public class StickodeCompletionContributor {
    public void StickodeCompletionContributor(String language, @NotNull CompletionParameters parameters,
                                              ProcessingContext context,
                                              @org.jetbrains.annotations.NotNull @NotNull CompletionResultSet result){
        // HandleRemaingContributors 를 통해 사용자가 타이핑하는 상황을 계속 확인하며 자동완성을 할지 말지 결정한다.
        HandleRemaingContributors hrc = new HandleRemaingContributors();
        hrc.handleremainingcontributors(parameters, result);

        // Top100 코드, 사용자 즐겨찾기의 자동완성 옵션의 레이아웃을 선언해준다
        AddNewLookupElementBasicOffer anle  = new AddNewLookupElementBasicOffer();
        AddNewLookupElementUser uanle = new AddNewLookupElementUser();

        //플러그인 DB에 접근하기 위한 state 선언
        PlugInState ps = PlugInStateSettings.getInstance().getState();

        // 플러그인 민감 DB 에서 Top100 코드 사용여부를 체크한다.
        SensitiveData GCF = new SensitiveData();
        String isUsingTopCode = GCF.isTopCode();
        String isUsingDividedCode = GCF.isDividedCode();

        if(ps != null) {
            //Top100 코드를 사용할 경우에 Top100 코드를 자동완성 옵션으로 만들어준다.
            if(!isUsingTopCode.equals("0")) {
                int code_data_size = ps.getStickodeTopData().size();
                if(isUsingDividedCode.equals("1")) {
                    for (int i = 0; i < code_data_size; i++) {
                        String code_name="";
                        String code_contents="";
                        String code_description="";
                        //인터넷 연결이 끊겨 코드데이터를 못가져왔을 경우 데이터가 null되는것을 방지
                        if (ps.getStickodeTopData().get(i) != null) {
                            for (int k = 0; k < ps.getStickodeTopData().get(i).size(); k++) {
                                if (ps.getStickodeTopData().get(i).get(k).get("lan").equals(language)) {
                                /*
                                플러그인 DB 에서 Top100 코드의
                                코드 / 이름 / 설명
                                을 가져와 자동완성의 옵션에 넣어준다
                                */
                                    code_name = ps.getStickodeTopData().get(i).get(k).get("name");
                                    code_contents = ps.getStickodeTopData().get(i).get(k).get("contents");
                                    code_description = ps.getStickodeTopData().get(i).get(k).get("description");
                                    anle.BasicOfferAddNewLookupElement(result, code_name, code_contents, " ("+code_description+")","divided",language);
                                }
                            }
                        }
                    }
                }
                else if(isUsingDividedCode.equals("0")) {
                    for (int i = 0; i < code_data_size - 1; i++) {
                        String code_name = "";
                        String code_contents = "";
                        String code_description = "";
                        //인터넷 연결이 끊겨 코드데이터를 못가져왔을 경우 데이터가 null되는것을 방지
                        if (ps.getStickodeTopData().get(i) != null) {
                            for (int k = 0; k < ps.getStickodeTopData().get(i).size(); k++) {
                                if (ps.getStickodeTopData().get(i).get(k).get("lan").equals(language)) {
                                /*
                                플러그인 DB 에서 Top100 코드의
                                코드 / 이름 / 설명
                                을 가져와 자동완성의 옵션에 넣어준다
                                */
                                    if (i > 1) {
                                        if (ps.getStickodeTopData().get(i - 1).get(0).get("name").equals(ps.getStickodeTopData().get(i).get(0).get("name")) && ps.getStickodeTopData().get(i - 1).get(0).get("lan").equals(ps.getStickodeTopData().get(i).get(0).get("lan")) || ps.getStickodeTopData().get(i - 2).get(0).get("name").equals(ps.getStickodeTopData().get(i).get(0).get("name")) && ps.getStickodeTopData().get(i - 2).get(0).get("lan").equals(ps.getStickodeTopData().get(i).get(0).get("lan"))) {
                                            code_name = ps.getStickodeTopData().get(i).get(k).get("name");
                                            code_description = ps.getStickodeTopData().get(i).get(k).get("description");
                                        } else {
                                            code_name = ps.getStickodeTopData().get(i).get(k).get("name");
                                        }
                                    }
                                    code_contents = code_contents + "\n" + ps.getStickodeTopData().get(i).get(k).get("contents");


                                }
                            }
                            if (code_description.equals("")) {
                                anle.BasicOfferAddNewLookupElement(result, code_name, code_contents, code_description, "",language);
                            } else {
                                anle.BasicOfferAddNewLookupElement(result, code_name, code_contents, " (" + code_description + ")", "",language);
                            }

                        }
                    }
                }
            }
            // 사용자가 즐겨찾기한 코드의 자동완성을 구현한다.
            int user_code_data_size = ps.getUserCustomCode().size();
            if(isUsingDividedCode.equals("1")) {
                for (int i = 0; i < user_code_data_size; i++) {
                    String code_name = "";
                    String code_contents = "";
                    String code_description = "";
                    //인터넷 연결이 끊겨 코드데이터를 못가져왔을 경우 데이터가 null되는것을 방지
                    if (ps.getUserCustomCode().get(i) != null) {
                        for (int k = 0; k < ps.getUserCustomCode().get(i).size(); k++) {
                            if (ps.getUserCustomCode().get(i).get(k).get("lan").equals(language)) {
                            /*
                            플러그인 DB 에서 유저가 즐겨찾기한 코드의
                            코드 / 이름 / 설명
                            을 가져와 자동완성의 옵션에 넣어준다
                            */
                                code_name = ps.getUserCustomCode().get(i).get(k).get("name");
                                code_contents = ps.getUserCustomCode().get(i).get(k).get("contents");
                                code_description = ps.getUserCustomCode().get(i).get(k).get("description");
                                uanle.UserAddNewLookupElement(result, code_name, code_contents, " ("+code_description+")","divided",language);
                            }
                        }
                    }
                }
            }else if(isUsingDividedCode.equals("0")) {
                for (int i = 0; i < user_code_data_size; i++) {
                    String code_name = "";
                    String code_contents = "";
                    String code_description = "";
                    //인터넷 연결이 끊겨 코드데이터를 못가져왔을 경우 데이터가 null되는것을 방지
                    if (ps.getUserCustomCode().get(i) != null) {
                        for (int k = 0; k < ps.getUserCustomCode().get(i).size(); k++) {
                            if (ps.getUserCustomCode().get(i).get(k).get("lan").equals(language)) {
                            /*
                            플러그인 DB 에서 유저가 즐겨찾기한 코드의
                            코드 / 이름 / 설명
                            을 가져와 자동완성의 옵션에 넣어준다
                            */
                                if (i > 1) {
                                    if (ps.getUserCustomCode().get(i - 1).get(0).get("name").equals(ps.getUserCustomCode().get(i).get(0).get("name")) && ps.getUserCustomCode().get(i - 1).get(0).get("lan").equals(ps.getUserCustomCode().get(i).get(0).get("lan")) || ps.getUserCustomCode().get(i - 2).get(0).get("name").equals(ps.getUserCustomCode().get(i).get(0).get("name")) && ps.getUserCustomCode().get(i - 2).get(0).get("lan").equals(ps.getUserCustomCode().get(i).get(0).get("lan"))) {
                                        code_name = ps.getUserCustomCode().get(i).get(k).get("name");
                                        code_description = ps.getUserCustomCode().get(i).get(k).get("description");
                                    } else {
                                        code_name = ps.getUserCustomCode().get(i).get(k).get("name");
                                    }
                                }
                                code_contents = code_contents + "\n" + ps.getUserCustomCode().get(i).get(k).get("contents");

                            }
                        }
                        if (code_description.equals("")) {
                            uanle.UserAddNewLookupElement(result, code_name, code_contents, code_description, "",language);
                        } else {
                            uanle.UserAddNewLookupElement(result, code_name, code_contents, " (" + code_description + ")", "",language);
                        }
                    }
                }
            }
        }
    }
}
